import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class PatientRegistryTest {
    PatientRegistry patientRegistry;
    Patient patient1;
    Patient patient2;

    @Before
    public void setUp() {
        this.patientRegistry = new PatientRegistry(true);
        patient1 = new Patient("Andrø", "Tanker", "Maren G. Skake", "29104300764", null);
        patientRegistry.registerPatient(patient1);
        patient2 = new Patient("Ane", "Rikke", "Dr. Klø", "12073004096", null);
    }

    @Test
    public void registerPatientTest() {
        assertFalse(patientRegistry.getPatients().contains(patient2));
        assertTrue(patientRegistry.registerPatient(patient2));
        assertTrue(patientRegistry.getPatients().contains(patient2));

        assertFalse(patientRegistry.registerPatient(patient1));
        assertFalse(patientRegistry.registerPatient(null));
    }

    @Test
    public void removePatientTest() {
        assertTrue(patientRegistry.getPatients().contains(patient1));
        assertTrue(patientRegistry.removePatient(patient1));
        assertFalse(patientRegistry.getPatients().contains(patient1));

        assertFalse(patientRegistry.removePatient(patient1));
        assertFalse(patientRegistry.removePatient(null));
    }

    @Test
    public void writeAndReadFromCSVTest() {
        File file = new File("testdata");
        ArrayList<Patient> oldPatients = new ArrayList<>(patientRegistry.getPatients());

        patientRegistry.writeToCSV(file);

        patientRegistry.getPatients().clear();
        assertEquals(0, patientRegistry.getPatients().size());

        patientRegistry.readFromCSV(file);

        assertEquals(oldPatients, patientRegistry.getPatients());

        assertTrue(file.delete());
    }

    @Test
    public void saveAndLoadFromDatabase() {
        ArrayList<Patient> oldPatients = new ArrayList<>(patientRegistry.getPatients());

        patientRegistry.saveToDatabase();

        patientRegistry.getPatients().clear();
        assertEquals(0, patientRegistry.getPatients().size());

        patientRegistry.loadFromDatabase();

        assertEquals(oldPatients, patientRegistry.getPatients());

    }

}
