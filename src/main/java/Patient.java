import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Patient {
    @Id
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String generalPractitioner;
    private String diagnosis;

    public Patient() {

    }

    public Patient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber, String diagnosis) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.generalPractitioner = generalPractitioner;
        this.socialSecurityNumber = socialSecurityNumber;
        this.diagnosis = diagnosis;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public String[] getAttributeArray() {
        return new String[] {this.firstName, this.lastName, this.generalPractitioner, this.socialSecurityNumber};
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(firstName, patient.firstName) &&
                Objects.equals(lastName, patient.lastName) &&
                Objects.equals(generalPractitioner, patient.generalPractitioner) &&
                Objects.equals(socialSecurityNumber, patient.socialSecurityNumber) &&
                Objects.equals(diagnosis, patient.diagnosis);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, generalPractitioner, socialSecurityNumber, diagnosis);
    }
}
