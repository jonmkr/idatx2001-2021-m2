import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.Optional;

/**
 * A JavaFX application for displaying and manipulating a PatientRegistry object
 */
public class PatientApplication extends Application {
    private final PatientRegistry patientRegistry = new PatientRegistry(false);
    private final ObservableList<Patient> patientList = FXCollections.observableArrayList();


    @FXML
    private TableView<Patient> tableView;
    @FXML
    private TableColumn<Patient, String> firstNameColumn;
    @FXML
    private TableColumn<Patient, String> lastNameColumn;
    @FXML
    private TableColumn<Patient, String> socialSecurityNumberColumn;

    @FXML
    private Label statusLabel;

    /**
     * Retrieves a file location from the user and saves the current registry as a CSV file.
     * @param event The event that triggered the method
     */
    @FXML
    void onExportToCSV(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export patient data");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV File", "*.csv"));

        File selectedFile = fileChooser.showSaveDialog(null);

        if (selectedFile == null) return;

        if (patientRegistry.writeToCSV(selectedFile)) {
            updateStatus("CSV exported", 1500);
        } else {
            updateStatus("An error occurred while writing to CSV", 2000);
        }

    }

    /**
     * Retrieves a file location from the user and loads registry data from the given CSV file.
     * @param event The event that triggered the method
     */
    @FXML
    void onImportFromCSV(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Import patient data");
        File selectedFile;

        while (true) {
            selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile == null) return;

            String[] splitFileName = selectedFile.getName().split("\\.");

            if (!splitFileName[splitFileName.length - 1].equals("csv")) {
                Dialog<ButtonType> dialog = new Dialog<>();
                dialog.setTitle("Invalid file type");
                dialog.setContentText("The chosen file type is not valid. Please choose another file by clicking on Select File or cancel the operation.");
                ButtonType selectFile = new ButtonType("Select File", ButtonData.NEXT_FORWARD);
                ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

                dialog.getDialogPane().getButtonTypes().addAll(selectFile, cancel);
                Optional<ButtonType> result = dialog.showAndWait();
                if (result.isEmpty() || result.get() == cancel)
                    return;

            } else {
                break;
            }
        }

        if (patientRegistry.readFromCSV(selectedFile)) {
            updateStatus("CSV imported", 1500);
            updateItems();
        } else {
            updateStatus("An error occurred while reading from CSV", 2000);
        }
    }

    /**
     * Loads the patient registry from the associated persistence unit.
     * @param event The event that triggered the method
     */
    @FXML
    void onLoadDatabase(ActionEvent event) {
        if (patientRegistry.loadFromDatabase()) {
            updateStatus("Loaded from database", 1500);
            updateItems();
        } else {
            updateStatus("An error occurred while loading from database", 2000);
        }
    }

    /**
     * Saves the patient registry to the associated persistence unit.
     * @param event The event that triggered the method
     */
    @FXML
    void onSaveDatabase(ActionEvent event) {
        if (patientRegistry.saveToDatabase())  {
            updateStatus("Saved to database", 1500);
        } else {
            updateStatus("An error occurred while saving to database", 2000);
        }
    }

    @FXML
    void onExit(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Opens a new patient management dialog with a new patient.
     * Registers patient after dialog box is closed with a affirmative result.
     * @param event The event that triggered the method
     */
    @FXML
    void onPatientAdd(ActionEvent event) {
        Patient patient = new Patient();
        Optional<Boolean> result = createPatientDialog(patient).showAndWait();
        if (result.isPresent() && result.get()) {

            if (patientRegistry.registerPatient(patient)) {
                updateStatus("Added " + patient.getFullName(), 1500);
                updateItems();
            } else {
                updateStatus("Addition failed", 2000);
            }
        }
    }

    /**
     * Opens a new patient management dialog with an existing patient.
     * Updates items and displays status message on affirmative result.
     * @param event The event that triggered the method
     */
    @FXML
    void onPatientEdit(ActionEvent event) {
        Patient patient = tableView.getSelectionModel().getSelectedItem();
        if (patient == null) return;

        Optional<Boolean> result = createPatientDialog(patient).showAndWait();
        if (result.isPresent() && result.get()) {
            updateStatus("Updated " + patient.getFullName(), 1500);
            updateItems();
        }
    }

    /**
     * Removes the currently selected patient from the patient registry
     * @param event The event that triggered the method
     */
    @FXML
    void onPatientRemove(ActionEvent event) {
        Patient patient = tableView.getSelectionModel().getSelectedItem();
        if (patient == null) return;

        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setTitle("Delete Confirmation");
        dialog.setContentText("Are you sure you want to delete " + patient.getFullName() + "?");
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.YES, ButtonType.CANCEL);

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.YES) {

            if (patientRegistry.removePatient(patient)) {
                updateStatus("Removed " + patient.getFullName(), 1500);
                updateItems();
            } else {
                updateStatus("Removal failed", 2000);
            }
        }
    }

    @FXML
    void onAbout(ActionEvent event) {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setTitle("About");
        dialog.setHeaderText("Patients Application\nv0.1-SNAPSHOT");
        dialog.setContentText("A recklessly assembled TaskView application\n\n(C) Jon Martin Kristiansen\n2021");
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.showAndWait();
    }

    @FXML
    void initialize() {
        // Configures each column to display the appropriate data
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        // Wraps the patientRegistry ArrayList in two TransformableList, the binds it to the TableView
        updateItems();
        SortedList<Patient> sortedPatientList = new SortedList<>(patientList);
        sortedPatientList.comparatorProperty().bind(tableView.comparatorProperty());
        tableView.setItems(sortedPatientList);
    }

    // Called every time a change is made to the patientRegistry ArrayList to update the TableView
    void updateItems() {
        patientList.setAll(patientRegistry.getPatients());
    }

    /**
     * Displays a timed message for the user at the bottom of the application
     * @param message The message to be displayed after a "Status:" prefix
     * @param time The time in milliseconds that the message should be shown for before reverting to "Status: OK"
     */
    void updateStatus(String message, int time) {
        Task<Void> task = new Task<>() {
            @Override
            protected Void call() throws Exception {
                updateMessage("Status: " + message);
                Thread.sleep(time);
                updateMessage("Status: OK");
                return null;
            }
        };

        Thread thread = new Thread(task);
        statusLabel.textProperty().bind(task.messageProperty());
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Creates a new patient management dialog for editing patient objects.
     * @param patient The patient object to be edited
     * @return true if patient object was successfully edited, false if the edit was cancelled
     */
    Dialog<Boolean> createPatientDialog(Patient patient) {
        Dialog<Boolean> dialog = new Dialog<>();
        dialog.setTitle("Patient Information");

        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        gridPane.add(new Label("First Name: "), 0, 0);
        TextField firstNameTextField = new TextField();
        firstNameTextField.setPromptText("First Name");
        firstNameTextField.setText(patient.getFirstName());
        gridPane.add(firstNameTextField, 1, 0);

        gridPane.add(new Label("Last Name: "), 0, 1);
        TextField lastNameTextField = new TextField();
        lastNameTextField.setPromptText("Last Name");
        lastNameTextField.setText(patient.getLastName());
        gridPane.add(lastNameTextField, 1, 1);

        gridPane.add(new Label("Social Security Number"), 0, 2);
        TextField socialSecurityNumberTextField = new TextField();
        socialSecurityNumberTextField.setPromptText("Social Security Number");
        socialSecurityNumberTextField.setText(patient.getSocialSecurityNumber());
        gridPane.add(socialSecurityNumberTextField, 1, 2);

        dialog.getDialogPane().setContent(gridPane);
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == ButtonType.OK) {
                patient.setFirstName(firstNameTextField.getText());
                patient.setLastName(lastNameTextField.getText());
                patient.setSocialSecurityNumber(socialSecurityNumberTextField.getText());
                return true;
            }
            return false;
        });

        return dialog;
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/rootStage.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root, 600, 400);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}