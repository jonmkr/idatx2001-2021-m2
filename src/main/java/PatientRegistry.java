import javax.persistence.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Registry of Patient objects
 */
public class PatientRegistry {
    private final ArrayList<Patient> patients;
    private final EntityManager entityManager;

    /**
     * Instantiates a new PatientRegistry with an empty patients ArrayList and Derby persistence unit connection
     * @param test Decides whether to use the test unit or production unit
     */
    public PatientRegistry(boolean test) {
        patients = new ArrayList<>();

        String persistenceUnit = test ? "test-register" : "st-olavs-register";
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
        entityManager = emf.createEntityManager();

    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Adds a patient to the patient registry
     * @param patient The patient being registered
     * @return true if patient is patient was successfully, false if patient was null or already registered
     */
    public boolean registerPatient(Patient patient) {
        if (patient != null && !patients.contains(patient)) {
            return patients.add(patient);
        } else {
            return false;
        }
    }

    /**
     * Removes a patient from the patient registry
     * @param patient The patient being removed
     * @return true if patient was successfully removed, false if patient not in registry
     */
    public boolean removePatient(Patient patient) {
        if (patients.contains(patient)) {
            return patients.remove(patient);
        } else {
            return false;
        }
    }

    /**
     * Reads data from a CSV file and updates the current patient registry
     * @param file The File to read from
     * @return true if CSV file was successfully loaded, false if file was not found
     */
    public boolean readFromCSV(File file) {
        try (Scanner sc = new Scanner(file.getAbsoluteFile())) {
            sc.useDelimiter("[;\n]");
            patients.clear();
            while (sc.hasNext()) {
                patients.add(new Patient(sc.next(), sc.next(), sc.next(), sc.next(), null));
            }
        } catch (FileNotFoundException e) {
            return false;
        }
        return true;
    }

    /**
     * Writes the current patient registry as data to a CSV file
     * @param file The File to write to
     * @return true if the CSV file was successfully written to, false if file was not found
     */
    public boolean writeToCSV(File file) {
        try (PrintWriter pw = new PrintWriter(file)) {
            for (Patient patient: patients) {
                pw.print(String.join(";", patient.getAttributeArray()) + "\n");
            }
        } catch (FileNotFoundException e) {
            return false;
        }
        return true;
    }

    /**
     * Stores current patient registry to a Derby persistence unit.
     * @return true if storage to the persistence unit was successful, false if an error occurred
     */
    public boolean saveToDatabase() {
        EntityTransaction transaction = null;
        try {
            transaction = entityManager.getTransaction();
            transaction.begin();
            Query query = entityManager.createQuery("DELETE FROM Patient");
            query.executeUpdate();
            for (Patient patient: patients) {
                entityManager.persist(patient);
            }
            transaction.commit();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
            return false;
        }
    }

    /**
     * Loads patient data from a Derby persistence unit and updates the current patient registry
     * @return true if patient data was loaded successfully, false if an exception occurred.
     */
    public boolean loadFromDatabase() {
        try {
            Query query = entityManager.createQuery("SELECT p FROM Patient p");
            patients.clear();
            patients.addAll(query.getResultList());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
