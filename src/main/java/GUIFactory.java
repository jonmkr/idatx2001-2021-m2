import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

public abstract class GUIFactory {
    public static Node getNode(String nodeType) {
        if (nodeType.equalsIgnoreCase("MENUBAR")) {
            return createMenuBar();
        } else if (nodeType.equalsIgnoreCase("TABLEVIEW")) {
            return createTableView();
        } else if (nodeType.equalsIgnoreCase("BUTTONBAR")) {
            return createButtonBar();
        } else if (nodeType.equalsIgnoreCase("STATUSFIELD")) {
            return createStatusField();
        }

        return null;
    }

    private static Node createMenuBar() {
        Menu file = new Menu("File");
        file.getItems().addAll(
                new MenuItem("Import From .CVS..."),
                new MenuItem("Export to .CVS..."),
                new SeparatorMenuItem(),
                new MenuItem("Exit"));

        Menu edit = new Menu("Edit");
        edit.getItems().addAll(
                new MenuItem("Add New Patient"),
                new MenuItem("Edit Selected Patient"),
                new MenuItem("Remove Selected Patient")
        );

        Menu help = new Menu("Help");
        help.getItems().add(new MenuItem("About"));

        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(file, edit, help);

        return menuBar;
    }

    private static Node createTableView() {
        TableView<Patient> tableView = new TableView<>();

        TableColumn<Patient, String> firstNameColumn = new TableColumn<>("First Name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Patient, String> lastNameColumn = new TableColumn<>("Last Name");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        TableColumn<Patient, String> SSNColumn = new TableColumn<>("Social security number");
        SSNColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        tableView.getColumns().addAll(firstNameColumn, lastNameColumn, SSNColumn);

        return tableView;


    }

    private static Node createButtonBar() {
        ButtonBar buttonBar = new ButtonBar();
        buttonBar.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
        buttonBar.setButtonMinWidth(30);
        VBox.setMargin(buttonBar, new Insets(3, 0, 3, 5));

        Button add = new Button();
        add.setGraphic(new ImageView("/addIcon.png"));

        Button edit = new Button();
        edit.setGraphic(new ImageView("/editIcon.png"));

        Button remove = new Button();
        remove.setGraphic(new ImageView("/removeIcon.png"));

        buttonBar.getButtons().addAll(remove, edit, add);

        return buttonBar;
    }

    private static Node createStatusField() {
        AnchorPane anchorPane = new AnchorPane();
        anchorPane.getChildren().add(new Label("Status: OK"));
        anchorPane.setStyle("-fx-background-color: grey;");
        return anchorPane;

    }

}
